var grid=[];

$(function(){
function Cell(width,height){
	this.width=width;
	this.height=height;
}

Cell.prototype.create=function(x, y){
	 var item=$("<div class='cell'><div></div></div>");
	 $("#externalGrid").append(item);
	 item.css({"width":this.width,"height":this.height});
	 item.css({left: x + 'px', top: y + 'px'});
	 this.el = item;
}

var createOne=function(x, y){
	var cell=new Cell(50,50);
	cell.create(x, y);
	return cell;
}


function randomNumber(n){
 return Math.floor(Math.random()*(n || 9));
}

var nums;
var solution;
function generateGame(){
	nums=[
	[1,2,3,4,5,6,7,8,9],
	[4,5,6,7,8,9,1,2,3],
	[7,8,9,1,2,3,4,5,6],
	[2,3,1,5,6,4,8,9,7],
	[5,6,4,8,9,7,2,3,1],
	[8,9,7,2,3,1,5,6,4],
	[3,1,2,6,4,5,9,7,8],
	[6,4,5,9,7,8,3,1,2],
	[9,7,8,3,1,2,6,4,5],
	];

	function change1(){
		var row1=randomNumber(3)*3;//0 3 6
		var row2=randomNumber(3)*3;
		console.log(row1,row2);
		var a=null;
		for(var i=0;i<3;i++){
			a=nums[row1+i];
			nums[row1+i]=nums[row2+i];
			nums[row2+i]=a;
		}
	}

		function change3(){
		var row1=randomNumber(3)*3;//0 3 6
		var row2=randomNumber(3)*3;
		console.log(row1,row2);
		var a=null;
		for(var i=0;i<3;i++){
			for(var j=0;j<9;j++)
			{
				a=nums[j][row1+i];
				nums[j][row1+i]=nums[j][row2+i];
				nums[j][row2+i]=a;
			}
		}
	}
	//сначала выбираем один из трех больших потом в нем один из двух маленьких
	function change2(){
		var bigRow=randomNumber(3)*3;//0 3 6
		var row1=randomNumber(3)+bigRow;//+1 или +2  пределах одной большой
		var row2=randomNumber(3)+bigRow;
	    {
			a=nums[row1];
			nums[row1]=nums[row2];
			nums[row2]=a;
		}
	}

	function change4(){
		var bigRow=randomNumber(3)*3;//0 3 6
		var row1=randomNumber(3)+bigRow;//+1 или +2  пределах одной большой
		var row2=randomNumber(3)+bigRow;
		for(var i=0;i<9;i++)
	    {
			a=nums[i][row1];
			nums[i][row1]=nums[i][row2];
			nums[i][row2]=a;
		}
	}

	for(var i=0;i<50;i++){
				change1();
				change2();
				change3();
				change4();
	}

	// сохранить решение отдельно
	solution = [];
	for (var x=0; x<9; x++) {
		solution[x] = [];
		for (var y=0; y<9; y++) {
			solution[x][y] = nums[x][y];
		}
	}

	//выбираем 30 которые удалить для решения пользователем
	for(var n=0;n<70;n++){
		var x=randomNumber(9);
		var y=randomNumber(9);
		nums[x][y]=null;
	}

};

function startGame(){
$(".cell").remove();
for (var i=0;i<9;i++) {
	grid[i] = [];
	 for (var j=0;j<9;j++)
	{
		var x = i * 50;
		var y = j * 50;
		grid[i][j] = createOne(x, y);
		grid[i][j].el.data({x: i, y: j});
	}
}
for(var x=0;x<9;x++){
	     for(var y=0;y<9;y++){
		var num=nums[x][y];
		if (num!==null){
	var errors = rules(grid, num, x, y);
	if (errors.length === 0)
	{
		grid[x][y].fixed = true;
		addToGrid(grid, num, x, y);
		/*
		grid[x][y].num = num;
		grid[x][y].el.text(grid[x][y].num);
		*/
		grid[x][y].el.addClass("firstly");
	}
}
}
}
$("#banner,#win,.gray,#lose").fadeOut();

};

$(document).on("click","#start",function(){
	generateGame();
	startGame();
});

$(document).on("click","#restart",function(){
	startGame();
});

function rule1(grid,num,x,y){
	var error1=[];
	for(var ty=0;ty<9;ty++){
		if (num===grid[x][ty].num)
		{
			error1.push({x: x, y: ty});
		}
	}
	return error1;
}

function rule2(grid,num,x,y){
	var error2=[];
	for(var tx=0;tx<9;tx++){
		if (num===grid[tx][y].num)
		{
			error2.push({x: tx, y: y});
		}
	}
	return error2;
}

function rule3(grid,num,x,y){
	var error3=[];
	//получкение левой верхней клеточки в каждом большом квадрате
	var tx = Math.floor(x/3)*3;
	var ty = Math.floor(y/3)*3;;
	for(var n=tx;n<tx+3;n++){
		for(var j=ty;j<ty+3;j++){
			if(num===grid[n][j].num)
			{
				error3.push({x: n, y: j});
			}
		}
  }
  return error3;
}

function rules(grid, i, x, y){
	  	 	var errors1 = rule1(grid,i,x,y);
	  	 	var errors2 = rule2(grid,i,x,y);
	  	 	var errors3 = rule3(grid,i,x,y);
	  	 	return errors1.concat(errors2).concat(errors3);
}

function selection(x,y){
		$(".selection").removeClass("selection");
	  	for (var ty=0;ty<9;ty++){
		  	 		grid[x][ty].el.addClass("selection")
		  	 	}
		  	 	for (var tx=0;tx<9;tx++){
		  	 		grid[tx][y].el.addClass("selection")
		  	 	}
		  	 }

  //подсветка рядов от выбранной клетки
$(document).on("mouseenter",".cell",function(){
		var x=$(this).data('x');
	var y=$(this).data('y');
	
	if ($('.selected').length === 0){
		selection(x,y);
	  }
	});

$(document).on("click","body",function(){
	$(".selected").removeClass("selected");
	$(".selection").removeClass("selection");
	$(".cell").removeClass("error");
});


function possibleMoves(grid,x,y){
	var possible=[];
	for(var i=1;i<=9;i++){
		if(rules(grid,i,x,y).length==0)
			possible.push(i);
	}
	return possible;
}
//подсказывает по одной в случайном порядке если вариант единственный
function help(){
	var hints = [];
	for(var x=0;x<9;x++){
		for(var y=0;y<9;y++){
			var nums=possibleMoves(grid,x,y);
			if (nums.length == 1 && grid[x][y].num===undefined){
				hints.push({x:x,y:y,num:nums[0]});
			}
		}
	}
	if (hints.length) {
		var hint = hints[randomNumber(hints.length)];
		var x = hint.x;
		var y = hint.y;
		addToGrid(grid,hint.num,x,y);
	}
}
$(document).on("click","#help", help);

function solve(){
	help();
	if (hasEmptyCells()) {
		setTimeout(solve,100);
	}
}
// $(document).on("click","#solve-old", function(){
// 	$('#solve').data({clicked: true});
// 	setTimeout(solve,100);
// });
$(document).on("click","#solve", function(){
	$('#solve').data({clicked: true});
	for(var x=0;x<9;x++){
		for(var y=0;y<9;y++){
			if (grid[x][y].num===undefined){
				addToGrid(grid,solution[x][y],x,y);
			}
		}
	}
});

function hasEmptyCells(){
	var emptyCells=0;
	for(var x=0;x<9;x++){
		for(var y=0;y<9;y++){
			if (grid[x][y].num===undefined){
				emptyCells++;
				return true;
			}
		}
	}
return false;
}

function addToGrid(grid,num,x,y){
	    grid[x][y].num =num;
		grid[x][y].el.find('div').text(grid[x][y].num);
		grid[x][y].el.addClass('digit');
		//setTimeout(function(){ grid[x][y].el.removeClass('digit'); }, 500);



	if(!hasEmptyCells())
	{
		if($('#solve').data('clicked'))
			{
				$("#lose").fadeIn();
			}
		else
			$("#win").fadeIn();
	}
}

$(document).on("click",".cell",function(event){
	event.stopPropagation();
	var x=$(this).data('x');
	var y=$(this).data('y');
	if (!grid[x][y].fixed){
	  $(".selected").removeClass("selected");
	  $(".cell").removeClass("error");
	  $(this).addClass("selected");
	  $(document).off("keydown");
	  selection(x,y);
	
	  var possible = possibleMoves(grid, x, y);
	  console.log('possible moves', possible);
	  // //подсветка рядов от выбранной клетки   
		 //  	for (var ty=0;ty<9;ty++){
		 //  	 		grid[x][ty].el.addClass("selection")
		 //  	 	}
		 //  	 	for (var tx=0;tx<9;tx++){
		 //  	 		grid[tx][y].el.addClass("selection")
		 //  	 	}  
		  	 	
	  $(document).on("keydown",function(e){
			$(".cell").removeClass("error");
	  	 console.log(e);
	  	 for (var i=1;i<=9;i++){

          	if(e.key==i){
		  	 	var errors = rules(grid, i, x, y);

          		if  (errors.length === 0)
          		{
					addToGrid(grid,i,x,y);
          		}
	          	else
	          	{
	          		errors.forEach(function(item){
	          			grid[item.x][item.y].el.addClass('error');
	          		});
	          		
	          	}
	        }
      	}
	  });
	}
})

		
// tests

function testRule3_topleft() {
	var grid = [
		[{/*add there*/}, {}, {}],
		[{}, {}, {}],
		[{}, {}, {num: 1}],
		[],
		[],
		[],
		[],
		[],
		[],
	];
	console.assert(rule3(grid, 1, 0, 0).length > 0, '3 top left');
}
testRule3_topleft();

function testRule3_topright() {
	var grid = [
		[],
		[],
		[],
		[],
		[],
		[],
		[{}, {/*add there*/}, {}],
		[{}, {}, {}],
		[{}, {}, {num: 1}],
	];
	console.assert(rule3(grid, 1, 6, 1).length > 0, '3 top right');
}
testRule3_topright();

});
